#include "llmo/detour.h"
#include "llmo/fn.h"

llmo::hook::detour::hook_t<void( unsigned short id,
								 unsigned char type,
								 const char *title,
								 const char *text,
								 const char *leftBtn,
								 const char *rightBtn,
								 bool remote )> *hook;

class DialogInfo {
public:
	void Show( unsigned short id,
			   unsigned char type,
			   const char *title,
			   const char *text,
			   const char *leftBtn,
			   const char *rightBtn,
			   bool remote ) {
		std::string hkTitle = "[" + std::to_string( id ) + ":" + std::to_string( type ) + "]";
		hkTitle += title;

		auto ptr = (void *)hook->original;
		auto original = ( void( __thiscall * )( DialogInfo * self,
												unsigned short id,
												unsigned char type,
												const char *title,
												const char *text,
												const char *leftBtn,
												const char *rightBtn,
												bool remote ) ) ptr;
		original( this, id, type, hkTitle.c_str(), text, leftBtn, rightBtn, remote );
	}
};

[[maybe_unused]] class loader {
public:
	loader() noexcept {
		auto samp = llmo::mem::lib::find( "samp" );
		using hook_t = typename std::remove_pointer<decltype( hook )>::type;
		try {
			hook = new hook_t( (void *)( samp + 0x6F8C0 ), llmo::fn::fn2void( &DialogInfo::Show ) );
		} catch ( ... ) {}
	}

	~loader() { delete hook; }
} g_loader;